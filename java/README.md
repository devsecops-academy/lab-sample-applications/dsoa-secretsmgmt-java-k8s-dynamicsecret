# Running Locally

```
docker run -it --rm --mount type=bind,source="$(pwd)",target=/app -p 8011:8011 --link db openjdk:12-jdk-alpine /bin/sh
```

```
cd /app
./gradlew bootRun
```