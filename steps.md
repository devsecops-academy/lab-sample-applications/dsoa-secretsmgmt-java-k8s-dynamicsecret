<!-- 1. Lets start by deploying the application on kubernetes so we can see what the application looks like.
  kubectl apply -f mysql.yaml
  kubectl apply -f spring-deployment.yaml -->



1. Create a service account called vault-auth. Creating a service account will give pods an identity that can be used to authenticate against.

  kubectl create serviceaccount vault-auth

2. We have included a deployment file to apply the service account configuration

  kubectl apply --filename vault-auth-service-account.yml

Lets check to the configuration of the service account  

kubectl describe sa vault-auth

    ```
    Name:                vault-auth
    Namespace:           default
    Labels:              <none>
    Annotations:         <none>
    Image pull secrets:  <none>
    Mountable secrets:   vault-auth-token-rtcg6
    Tokens:              vault-auth-token-rtcg6
    Events:              <none>
    ```
  Here we can see that we have a service account called vault-auth running in the default namespace and the associated token. Your token will have a slightly different value.

3. We have also created a policy which restricts the usage of our secrets to read and list only.
Before you apply the policy, you will need to set the location of your vault and login.

export VAULT_ADDR=<YOUR_VAULT_ADDR>

vault login
```Token (will be hidden):
```
Now that you have logged in successfully, take a look at the vault policy under myapp-kv-ro.hcl

    ```
      # If working with K/V v1
      path "secret/myapp/*" {
        capabilities = ["read", "list"]
      }

      # If working with K/V v2
      path "secret/data/myapp/*" {
        capabilities = ["read", "list"]
      }
    ```
    Apply it using:
    vault policy write myapp-kv-ro myapp-kv-ro.hcl

    ```
      Success! Uploaded policy: myapp-kv-ro
    ```

4. Pods will typically authenticate against the name space that they are created in, however, in this case, we must specify that they authenticate against the vault-auth service account. In order to do that, we need to add the Service Account name to the Deployment in spring-deployment.yaml by adding the following below line 17  

```
  serviceAccountName: vault-auth
```
This ensures that our pod is looking for and authenticating to the Kubernetes API correctly.

5. Next, we need to set the vault-auth Service Account name,

Set VAULT_SA_NAME to the service account you created earlier:

  export VAULT_SA_NAME=$(kubectl get sa vault-auth -o jsonpath="{.secrets[*]['name']}")

the vault-auth Service Account JWT used to authenticate to the Kubernetes API:  

  export SA_JWT_TOKEN=$(kubectl get secret $VAULT_SA_NAME -o jsonpath="{.data.token}" | base64 --decode; echo)

the Service Account PEM encoded CA Cert used to talk to Kubernetes API:

  export SA_CA_CRT=$(kubectl get secret $VAULT_SA_NAME -o jsonpath="{.data['ca\.crt']}" | base64 --decode; echo)

and the Kubernetes Host (Please omit the http or https)

  export K8S_HOST=$(grep 'server:' Kubeconfig.yaml | awk '{print $2}')

6. Now flip over to your command line and enable the Kubernetes auth method in Vault

  vault auth enable kubernetes

7. Now configure Vault to use the kubernetes authentication information that you set in the previous step.

vault write auth/kubernetes/config \
        token_reviewer_jwt="$SA_JWT_TOKEN" \
        kubernetes_host="https://$K8S_HOST" \
        kubernetes_ca_cert="$SA_CA_CRT"

8. Now set the role and tell it which service account, namespace, and which polices it should use.

vault write auth/kubernetes/role/example \
        bound_service_account_names=vault-auth \
        bound_service_account_namespaces=default \
        policies=myapp-kv-ro \
        ttl=24h

9. Sanity check. Let's make sure the cluster and the vault can talk to each other.

Spin up a temp alpine container, update, and install curl and jq. Set the VAULT_ADDR and curl the health endpoint to check the Vault status.

kubectl run --generator=run-pod/v1 tmp --rm -i --tty \
    --serviceaccount=vault-auth --image alpine:3.7

    apk update
    apk add curl && jq

  VAULT_ADDR=<vault machine address>

  curl -s $VAULT_ADDR/v1/sys/health | jq

10. Now set the KUBE_TOKEN using the serviceaccount/token Kubernetes API and make a curl request to the Vault api kubernetes/login endpoint

  KUBE_TOKEN=$(cat /var/run/secrets/kubernetes.io/serviceaccount/token)

  echo $KUBE_TOKEN

  curl --request POST \
        --data '{"jwt": "'"$KUBE_TOKEN"'", "role": "example"}' \
        $VAULT_ADDR/v1/auth/kubernetes/login | jq


  If it is successful, you should see information about your token and even the policies attached.

11. Let's start configuring our application, now and put the static database credentials into Vault.

    vault kv put secret/myapp spring.datasource.username=82618201 spring.datasource.password=animism-sort-galvanic-angina-camp

 and check to make sure that the information can be properly retrieved

    vault kv get secret/myapp

12. Now, flip over to the application and require the spring boot vault libraries in the gradle build.
    In the root of the java app, find the build.gradle file and under dependencies add:
    ```
    implementation 'org.springframework.cloud:spring-cloud-starter-vault-config:2.1.2.RELEASE'
    implementation 'org.springframework.cloud:spring-cloud-vault-config-databases:2.1.2.RELEASE'
    ```
13. Under src/main/resources/application.properties remove the lines using the spring.datasource.password and spring.datasource.username from the properties. Yes, that's right. Delete them. Gone. If you don't trust me, you can comment them out. But you can trust me.

14. Under src/main/resources create a file called bootstrap.yml. Here we are going to add information so that our library knows where to call Vault, the location of the stored secrets in vault, the authentication method, and within Kuberenetes, which role and token to use for authentication.   

    ```
    spring.cloud.vault:
        uri: <YOUR_VAULT_ADDR>
        generic:
          enabled: false
          application-name: myapp
        authentication: KUBERNETES
        kubernetes:
            role: example
            service-account-token-file: /var/run/secrets/kubernetes.io/serviceaccount/token
```

15. Once this is done, you can rebuild the docker images and push them to the repository. Once they are ready, kill your atp-java-xxxxxx pod

kubectl delete po atp-java-xxxxxx

A new pod will spin up with the modified application.

16. Give it a few minutes for the application to deploy and then check your application url. If everything went as planned, you should see your application!

Dynamic Secret Vault Commands :

vault secrets enable database
   - vault write database/config/mysql plugin_name=mysql-database-plugin connection_url="{{username}}:{{password}}@tcp(<mysqlmachineloadbalancerip>:3306)/" allowed_roles="customer-portal-readonly" username="root" password="<root_password>"
   - vault write database/roles/customer-portal-readonly db_name=mysql creation_statements="CREATE USER '{{name}}'@'%' IDENTIFIED BY '{{password}}';GRANT SELECT ON *.* TO '{{name}}'@'%';" default_ttl="1h" max_ttl="24h" 
